''' This file forms part of the Urban-PLUMBER benchmarking evaluation project 
for urban areas. Copyright 2020. This program is distributed freely in the hope
that it will be useful, but with NO WARRANTY, implied or otherwise. Users are 
responsible for ensuring it is fit for their purpose. '''

__title__   = 'Urban-PLUMBER netCDF creation script'
__version__ = 'v1.04 (2020-06-19)'
__author__  = 'Mathew Lipson'
__email__   = 'm.lipson@unsw.edu.au'

'''
This python3 script constructs a netCDF file which is formatted to comply 
with the Urban-PLUMBER protocol and fills it with output model data.
Run the script "as-is" to create a netcdf from example model output.

Instructions:
    1. INPUT INFORMATION: update paths to your files and soil layer number
    2. GET OUTPUT: import your model output and change variables as required.
    3. SET OUTPUT: set model output into netcdf file.
    4. run script

Notes:
    standard_name attributes are based on CF conventions

Acknowledgments:
    With thanks Aristofanis Tsiringakis

Changelog:
    v1.01: Correct RootMoist attributes
    v1.02: add x,y dimensions to all variables to align with ALMA
    v1.03: change time datatype from i8 (64bit) to i4 (32bit), add history
    v1.04: added x,y dimension variable definitions
    v1.05: Add compression to example (reduces filesize by 2/3) and SWup/LWup.
    v1.06: Clarify "positive" upward/downward etc in long_name
    v1.07: Convert metadata timestep info to int
'''

import numpy as np
import pandas as pd
import netCDF4 as nc

missing_float = -9999.

# add sites to list as required
sitelist = ['XX-Test']

###############################################################################
##### MAIN: no action required
###############################################################################

def main(sitename):

    print('loading site data and forcing information')
    info = set_info(sitename)
    sitedata,info = set_more_info(info)

    print('building empty netcdf in complying form')
    create_empty_netcdf(info)

    print('reading model output (in this case from text file)')
    data = get_model_data(info)

    print('setting netcdf with output data')
    set_netcdf_data(data,info)

    print('done! see %s/%s' %(info['outpath'],info['fname_output']))
    
    return

###############################################################################
##### 1. INPUT INFORMATION: update paths to your files and soil layer number
###############################################################################

def set_info(sitename):
    '''sets user inputs'''
    print('processing %s' %sitename)

    info = {}

    # site name
    info['sitename'] = '%s' %sitename
  
    # path to site data 
    info['sitepath'] = './%s' %sitename

    # path to model output
    info['model_outpath'] = './%s/output/modeloutput_EXAMPLE.txt' %sitename
    
    # path to netcdf output
    info['outpath']  = './%s/output' %sitename

    # name of netcdf forcing file for site
    info['fname_forcing'] = '%s_metforcing_v1.nc' %sitename
    
    # name of netcdf output file:
    info['fname_output'] = 'EXAMPLE_%s_baseline_v1.nc' %sitename
    
    # list number of soil layers in model
    info['num_soil_layers']  = 4

    return info

###############################################################################
##### 2. GET OUTPUT: import your model output and change variables as required.
###############################################################################

def get_model_data(info):
    '''
    Reads model output (in this example text) into a pandas dataframe.
    Update the function to match your model output data types.

    Inputs
    ------
    info (dictionary): script information

    Outputs
    -------
    data (dataframe): model output
    '''

    # import model data from text form using pandas
    data = pd.read_csv( filepath_or_buffer=info['model_outpath'], 
                        delim_whitespace=True,
                        skiprows=3 )

    # make changes or additional variables as required
    data['SWnet'] = data['SWdown'] - data['SWup']
    data['LWnet'] = data['LWdown'] - data['LWup']

    # set timesteps using forcing information
    times = pd.date_range( start = info['time_coverage_start'], 
                           end   = info['time_coverage_end'], 
                           freq  = '%sS' %(info['timestep_interval_seconds']))
    data.index = times  # define time index in UTC

    return data

###############################################################################
##### 3. SET OUTPUT: set model output into netcdf file.
###############################################################################

def set_netcdf_data(data,info):
    '''
    Write model data values into existing netCDF file. 
    This function sets a small number of variables as an example. 
    Please include all available model output.
    Note subsurface state variables are two dimensional {time, soil_layer}.

    Inputs
    ------
    data (dataframe): model data
    info (dictionary): script information
    '''

    timesteps = int(info['timestep_number_spinup']) + int(info['timestep_number_analysis'])

    # define empty numpy arrays for missing data
    no_data_1D = np.full([ timesteps ], missing_float)
    no_data_2D = np.full([ timesteps, info['num_soil_layers'] ], missing_float)

    fname = '%s/%s' %(info['outpath'], info['fname_output'])

    # open netcdf files (r = read only, r+ = append existing)
    with nc.Dataset(filename=fname, mode='r+', format='NETCDF4') as o:

        # set metadata
        o.title             = '<Modelname> output for the Urban-PLUMBER project'
        o.site              = 'Site name (e.g. AU-Preston)'
        o.experiment        = '"Baseline" or "Detailed" experiment'
        o.institution       = 'Name of group submitting'
        o.primary_contact   = 'Name and email of primary contact person'
        o.secondary_contact = 'Name and email of secondary contact person'
        o.model             = 'Short name of model (9 or less characters)'
        o.source            = 'Full name of model and version'
        o.references        = 'Publication reference(s) for the model'
        o.repository        = 'A link to model code repository (e.g. github) if available'
        o.site_experience   = 'Has the group had previous experience modelling the site?'
        o.additional_data   = 'Has the group used additional site-specific data to configure the simulation?'
        o.comment           = 'Any additional comments participants wish to record'

        o.history           = 'Created with %s at %s' %(__file__,pd.Timestamp.now())

        # Critical energy balance components 
        o.variables['SWnet'][:]        = data['SWnet'].values       # Net shortwave radiation (downward)
        o.variables['LWnet'][:]        = data['LWnet'].values       # Net longwave radiation (downward)
        o.variables['Qle'][:]          = data['QEup'].values        # Latent heat flux (upward)
        o.variables['Qh'][:]           = data['QHup'].values        # Sensible heat flux (upward)
        o.variables['Qanth'][:]        = data['Qanth'].values       # Anthropogenic heat flux (upward)
        o.variables['Qstor'][:]        = data['dQS'].values         # Net storage heat flux in all materials (increase)
        o.variables['SWup'][:]         = data['SWup'].values        # Upwelling shortwave radiation flux (upward)
        o.variables['LWup'][:]         = data['LWup'].values        # Upwelling longwave radiation flux (upward)
        # Additional energy balance components
        o.variables['Qg'][:]           = no_data_1D   # Ground heat flux (downward)
        o.variables['Qanth_Qh'][:]     = no_data_1D   # Anthropogenic sensible heat flux (upward)
        o.variables['Qanth_Qle'][:]    = no_data_1D   # Anthropogenic latent heat flux (upward)
        o.variables['Qtau'][:]         = no_data_1D   # Momentum flux (downward)
        # General water balance components
        o.variables['Snowf'][:]        = no_data_1D   # Snowfall rate (downward)
        o.variables['Rainf'][:]        = no_data_1D   # Rainfall rate (downward)
        o.variables['Evap'][:]         = no_data_1D   # Total evapotranspiration (upward)
        o.variables['Qs'][:]           = no_data_1D   # Surface runoff (out of gridcell)
        o.variables['Qsb'][:]          = no_data_1D   # Subsurface runoff (out of gridcell)
        o.variables['Qsm'][:]          = no_data_1D   # Snowmelt (solid to liquid)
        o.variables['Qfz'][:]          = no_data_1D   # Re-freezing of water in the snow (liquid to solid)
        o.variables['DelSoilMoist'][:] = no_data_1D   # Change in soil moisture (increase)
        o.variables['DelSWE'][:]       = no_data_1D   # Change in snow water equivalent (increase)
        o.variables['DelIntercept'][:] = no_data_1D   # Change in interception storage (increase)
        o.variables['Qirrig'][:]       = no_data_1D   # Anthropogenic water flux from irrigation (increase)
        # Surface state variables
        o.variables['SnowT'][:]        = no_data_1D   # Snow surface temperature
        o.variables['VegT'][:]         = no_data_1D   # Vegetation canopy temperature
        o.variables['BaresoilT'][:]    = no_data_1D   # Temperature of bare soil (skin)
        o.variables['AvgSurfT'][:]     = no_data_1D   # Average surface temperature (skin)
        o.variables['RadT'][:]         = no_data_1D   # Surface radiative temperature
        o.variables['Albedo'][:]       = no_data_1D   # Surface albedo
        o.variables['SWE'][:]          = no_data_1D   # Snow water equivalent
        o.variables['SurfStor'][:]     = no_data_1D   # Surface water storage
        o.variables['SnowFrac'][:]     = no_data_1D   # Snow covered fraction
        o.variables['SAlbedo'][:]      = no_data_1D   # Snow albedo
        o.variables['CAlbedo'][:]      = no_data_1D   # Vegetation canopy albedo
        o.variables['UAlbedo'][:]      = no_data_1D   # Urban canopy albedo
        o.variables['LAI'][:]          = no_data_1D   # Leaf area index
        o.variables['RoofSurfT'][:]    = no_data_1D   # Roof surface temperature (skin)
        o.variables['WallSurfT'][:]    = no_data_1D   # Wall surface temperature (skin)
        o.variables['RoadSurfT'][:]    = no_data_1D   # Road surface temperature (skin)
        o.variables['TairSurf'][:]     = no_data_1D   # Near surface air temperature (2m)
        o.variables['TairCanyon'][:]   = no_data_1D   # Air temperature in street canyon (bulk)
        o.variables['TairBuilding'][:] = no_data_1D   # Air temperature in buildings (bulk)
        # Sub-surface state variables **** TWO DIMENSIONAL ****
        o.variables['SoilMoist'][:,:]  = no_data_2D   # Average layer soil moisture
        o.variables['SoilTemp'][:,:]   = no_data_2D   # Average layer soil temperature
        # Evaporation components
        o.variables['TVeg'][:]         = no_data_1D   # Vegetation transpiration
        o.variables['ESoil'][:]        = no_data_1D   # Bare soil evaporation
        o.variables['RootMoist'][:]    = no_data_1D   # Root zone soil moisture
        o.variables['SoilWet'][:]      = no_data_1D   # Total soil wetness
        o.variables['ACond'][:]        = no_data_1D   # Aerodynamic conductance
        # Forcing data (at forcing height)
        o.variables['SWdown'][:]       = no_data_1D   # Downward shortwave radiation
        o.variables['LWdown'][:]       = no_data_1D   # Downward longwave radiation
        o.variables['Tair'][:]         = no_data_1D   # Air temperature
        o.variables['Qair'][:]         = no_data_1D   # Specific humidity
        o.variables['PSurf'][:]        = no_data_1D   # Air pressure
        o.variables['Wind'][:]         = no_data_1D   # Wind speed

    return

###############################################################################
##### OTHER FUNCTIONS: These standard functions shouldn't need to be altered
###############################################################################

def set_more_info(info):
    '''sets timing and other info from forcing file and places into info dictionary.

    Inputs
    ------
    info (dictionary): script information

    Outputs
    -------
    info (dictionary): additional script information
    '''

    fpath = '%s/%s' %(info['sitepath'],info['fname_forcing'])

    with nc.Dataset(filename=fpath, mode='r', format='NETCDF4') as f:

        info['time_coverage_start']       = f.time_coverage_start
        info['time_coverage_end']         = f.time_coverage_end
        info['time_analysis_start']       = f.time_analysis_start
        info['local_utc_offset_hours']    = f.local_utc_offset_hours
        info['timestep_interval_seconds'] = f.timestep_interval_seconds
        info['timestep_number_spinup']    = f.timestep_number_spinup
        info['timestep_number_analysis']  = f.timestep_number_analysis

    # loading site parameters
    fpath = '%s/%s_sitedata_v1.csv' %(info['sitepath'], info['sitename'] )
    sitedata_full = pd.read_csv(fpath, index_col=1, delimiter=',')
    sitedata      = pd.to_numeric(sitedata_full['value'])

    info['latitude']      = sitedata['latitude']
    info['longitude']     = sitedata['longitude']

    return sitedata,info

###############################################################################

def create_empty_netcdf(info):
    '''creates empty netcdf dataset complying with Urban-PLUMBER protocol v1.0

    Inputs
    ------
    info (dictionary): script information

    '''

    timesteps = int(info['timestep_number_spinup']) + int(info['timestep_number_analysis'])

    fpath = '%s/%s' %(info['outpath'],info['fname_output'])

    # open netcdf files (r = read only, w = write new)
    with nc.Dataset(filename=fpath, mode='w', format='NETCDF4') as o:

        # setting coordinate values
        times         = [t*int(info['timestep_interval_seconds']) for t in range(0,int(timesteps))]
        soil_layers   = [i for i in range(1,info['num_soil_layers']+1)]

        ############ create dimensions ############
        o.createDimension(dimname='time', size=timesteps)
        o.createDimension(dimname='soil_layer', size=info['num_soil_layers'])
        o.createDimension(dimname='x', size=1)
        o.createDimension(dimname='y', size=1)

        ############ create coordinates ############
        var = 'time'
        o.createVariable(var, datatype='i4', dimensions=('time'), fill_value = missing_float)
        o.variables[var].long_name     = 'Time'
        o.variables[var].standard_name = 'time'
        o.variables[var].units         = 'seconds since %s' %info['time_coverage_start']
        o.variables[var].calendar      = 'standard'
        o.variables[var][:]            = times

        var = 'soil_layer'
        o.createVariable(var, datatype='i4', dimensions=('soil_layer'), fill_value = missing_float)
        o.variables[var].long_name     = 'Soil layer number'
        o.variables[var][:]            = soil_layers

        var = 'x'
        o.createVariable(var, datatype='i4', dimensions=('x'), fill_value = missing_float)
        o.variables[var].long_name     = 'x dimension'
        o.variables[var][:]            = 1

        var = 'y'
        o.createVariable(var, datatype='i4', dimensions=('y'), fill_value = missing_float)
        o.variables[var].long_name     = 'y dimension'
        o.variables[var][:]            = 1

        ################### latidude and longitude ###################

        var = 'longitude'
        o.createVariable(var, datatype='f8', dimensions=('y', 'x'), fill_value = missing_float)
        o.variables[var].long_name     = 'Longitude'
        o.variables[var].standard_name = 'longitude'
        o.variables[var].units         = 'degrees_east'
        o.variables[var][:]            = info['longitude']

        var = 'latitude'
        o.createVariable(var, datatype='f8', dimensions=('y', 'x'), fill_value = missing_float)
        o.variables[var].long_name     = 'Latitude'
        o.variables[var].standard_name = 'latitude'
        o.variables[var].units         = 'degrees_north'
        o.variables[var][:]            = info['latitude']

        ##########################################################################
        ################### critical energy balance components ###################

        var = 'SWnet'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Net shortwave radiation (positive downward)'
        o.variables[var].standard_name = 'surface_net_downward_shortwave_flux'
        o.variables[var].units         = 'W/m2'

        var = 'LWnet'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Net longwave radiation (positive downward)'
        o.variables[var].standard_name = 'surface_net_downward_longwave_flux'
        o.variables[var].units         = 'W/m2'

        var = 'Qle'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Latent heat flux (positive upward)'
        o.variables[var].standard_name = 'surface_upward_latent_heat_flux'
        o.variables[var].units         = 'W/m2'

        var = 'Qh'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Sensible heat flux (positive upward)'
        o.variables[var].standard_name = 'surface_upward_sensible_heat_flux'
        o.variables[var].units         = 'W/m2'

        var = 'Qanth'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Anthropogenic heat flux (positive upward)'
        o.variables[var].standard_name = 'surface_upward_heat_flux_due_to_anthropogenic_energy_consumption'
        o.variables[var].units         = 'W/m2'

        var = 'Qstor'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Net storage heat flux in all materials (increase)'
        o.variables[var].standard_name = 'surface_thermal_storage_heat_flux'
        o.variables[var].units         = 'W/m2'

        var = 'SWup'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Upwelling shortwave radiation flux (positive upward)'
        o.variables[var].standard_name = 'surface_upwelling_shortwave_flux_in_air'
        o.variables[var].units         = 'W/m2'

        var = 'LWup'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Upwelling longwave radiation flux (positive upward)'
        o.variables[var].standard_name = 'surface_upwelling_longwave_flux_in_air'
        o.variables[var].units         = 'W/m2'

        ################### additional energy balance compoenents #################

        var = 'Qg'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Ground heat flux (positive downward)'
        o.variables[var].standard_name = 'downward_heat_flux_at_ground_level_in_soil'
        o.variables[var].units         = 'W/m2'

        var = 'Qanth_Qh'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Anthropogenic sensible heat flux (positive upward)'
        o.variables[var].standard_name = 'surface_upward_sensible_heat_flux_due_to_anthropogenic_energy_consumption'
        o.variables[var].units         = 'W/m2'

        var = 'Qanth_Qle'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Anthropogenic latent heat flux (positive upward)'
        o.variables[var].standard_name = 'surface_upward_latent_heat_flux_due_to_anthropogenic_energy_consumption'
        o.variables[var].units         = 'W/m2'

        var = 'Qtau'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Momentum flux (positive downward)'
        o.variables[var].standard_name = 'magnitude_of_surface_downward_stress'
        o.variables[var].units         = 'N/m2'

        ##################### general water balance components #####################

        var = 'Snowf'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snowfall rate (positive downward)'
        o.variables[var].standard_name = 'snowfall_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Rainf'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Rainfall rate (positive downward)'
        o.variables[var].standard_name = 'rainfall_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Evap'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Total evapotranspiration (positive upward)'
        o.variables[var].standard_name = 'surface_evapotranspiration'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Qs'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Surface runoff (positive out of gridcell)'
        o.variables[var].standard_name = 'surface_runoff_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Qsb'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Subsurface runoff (positive out of gridcell)'
        o.variables[var].standard_name = 'subsurface_runoff_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Qsm'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snowmelt (solid to liquid)'
        o.variables[var].standard_name = 'surface_snow_and_ice_melt_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'Qfz'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Re-freezing of water in the snow (liquid to solid)'
        o.variables[var].standard_name = 'surface_snow_and_ice_refreezing_flux'
        o.variables[var].units         = 'kg/m2/s'

        var = 'DelSoilMoist'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Change in soil moisture (increase)'
        o.variables[var].standard_name = 'change_over_time_in_mass_content_of_water_in_soil'
        o.variables[var].units         = 'kg/m2'

        var = 'DelSWE'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Change in snow water equivalent (increase)'
        o.variables[var].standard_name = 'change_over_time_in_surface_snow_and_ice_amount'
        o.variables[var].units         = 'kg/m2'

        var = 'DelIntercept'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Change in interception storage (increase)'
        o.variables[var].standard_name = 'change_over_time_in_canopy_water_amount'
        o.variables[var].units         = 'kg/m2'

        var = 'Qirrig'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Anthropogenic water flux from irrigation (increase)'
        o.variables[var].standard_name = 'surface_downward_mass_flux_of_water_due_to_irrigation'
        o.variables[var].units         = 'kg/m2/s'

        ########################## surface state variables ########################

        var = 'SnowT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snow surface temperature'
        o.variables[var].standard_name = 'surface_snow_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'snow'

        var = 'VegT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Vegetation canopy temperature'
        o.variables[var].standard_name = 'surface_canopy_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'vegetation'

        var = 'BaresoilT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Temperature of bare soil'
        o.variables[var].standard_name = 'surface_ground_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'baresoil'

        var = 'AvgSurfT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Average surface temperature (skin)'
        o.variables[var].standard_name = 'surface_temperature'
        o.variables[var].units         = 'K'

        var = 'RadT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Surface radiative temperature'
        o.variables[var].standard_name = 'surface_radiative_temperature'
        o.variables[var].units         = 'K'

        var = 'Albedo'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Surface albedo'
        o.variables[var].standard_name = 'surface_albedo'
        o.variables[var].units         = '1'

        var = 'SWE'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snow water equivalent'
        o.variables[var].standard_name = 'surface_snow_amount'
        o.variables[var].units         = 'kg/m2'

        var = 'SurfStor'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Surface water storage'
        o.variables[var].standard_name = 'surface_water_amount_assuming_no_snow'
        o.variables[var].units         = 'kg/m2'

        var = 'SnowFrac'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snow covered fraction'
        o.variables[var].standard_name = 'surface_snow_area_fraction'
        o.variables[var].units         = '1'

        var = 'SAlbedo'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Snow albedo'
        o.variables[var].standard_name = 'snow_and_ice_albedo'
        o.variables[var].units         = '1'
        o.variables[var].subgrid       = 'snow'

        var = 'CAlbedo'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Vegetation canopy albedo'
        o.variables[var].standard_name = 'canopy_albedo'
        o.variables[var].units         = '1'
        o.variables[var].subgrid       = 'vegetation'

        var = 'UAlbedo'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Urban canopy albedo'
        o.variables[var].standard_name = 'urban_albedo'
        o.variables[var].units         = '1'
        o.variables[var].subgrid       = 'urban'

        var = 'LAI'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Leaf area index'
        o.variables[var].standard_name = 'leaf_area_index'
        o.variables[var].units         = '1'
        o.variables[var].subgrid       = 'vegetation'

        var = 'RoofSurfT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Roof surface temperature (skin)'
        o.variables[var].standard_name = 'surface_roof_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'roof'

        var = 'WallSurfT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Wall surface temperature (skin)'
        o.variables[var].standard_name = 'surface_wall_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'wall'

        var = 'RoadSurfT'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Road surface temperature (skin)'
        o.variables[var].standard_name = 'surface_road_skin_temperature'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'road'

        var = 'TairSurf'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Near surface air temperature (2m)'
        o.variables[var].standard_name = 'air_temperature_near_surface'
        o.variables[var].units         = 'K'

        var = 'TairCanyon'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Air temperature in street canyon (bulk)'
        o.variables[var].standard_name = 'air_temperature_in_street_canyon'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'canyon'

        var = 'TairBuilding'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Air temperature in buildings (bulk)'
        o.variables[var].standard_name = 'air_temperature_in_buildings'
        o.variables[var].units         = 'K'
        o.variables[var].subgrid       = 'building'

        ######################## Sub-surface state variables ######################

        var = 'SoilMoist'
        o.createVariable(var, datatype='f8', dimensions=('time','soil_layer','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Average layer soil moisture'
        o.variables[var].standard_name = 'moisture_content_of_soil_layer'
        o.variables[var].units         = 'kg/m2'

        var = 'SoilTemp'
        o.createVariable(var, datatype='f8', dimensions=('time','soil_layer','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Average layer soil temperature'
        o.variables[var].standard_name = 'soil_temperature'
        o.variables[var].units         = 'K'

        ########################## Evaporation components #########################

        var = 'TVeg'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Vegetation transpiration'
        o.variables[var].standard_name = 'transpiration_flux'
        o.variables[var].units         = 'kg/m2/s'
        o.variables[var].subgrid       = 'vegetation'

        var = 'ESoil'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Bare soil evaporation'
        o.variables[var].standard_name = 'liquid_water_evaporation_flux_from_soil'
        o.variables[var].units         = 'kg/m2/s'
        o.variables[var].subgrid       = 'baresoil'

        var = 'RootMoist'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Root zone soil moisture'
        o.variables[var].standard_name = 'mass_content_of_water_in_soil_defined_by_root_depth'
        o.variables[var].units         = 'kg/m2'

        var = 'SoilWet'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Total soil wetness'
        o.variables[var].standard_name = 'relative_soil_moisture_content_above_wilting_point'
        o.variables[var].units         = '1'

        var = 'ACond'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Aerodynamic conductance'
        o.variables[var].standard_name = 'inverse_aerodynamic_resistance'
        o.variables[var].units         = 'm/s'

        ########################## forcing data variables #########################

        var = 'SWdown'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Downward shortwave radiation at measurement height'
        o.variables[var].standard_name = 'surface_downwelling_shortwave_flux_in_air'
        o.variables[var].units         = 'W/m2'

        var = 'LWdown'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Downward longwave radiation at measurement height'
        o.variables[var].standard_name = 'surface_downwelling_longwave_flux_in_air'
        o.variables[var].units         = 'W/m2'

        var = 'Tair'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Air temperature at measurement height'
        o.variables[var].standard_name = 'air_temperature'
        o.variables[var].units         = 'K'

        var = 'Qair'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Specific humidity at measurement height'
        o.variables[var].standard_name = 'surface_specific_humidity'
        o.variables[var].units         = '1'

        var = 'PSurf'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Air pressure at measurement height'
        o.variables[var].standard_name = 'surface_air_pressure'
        o.variables[var].units         = 'Pa'

        var = 'Wind'
        o.createVariable(var, datatype='f8', dimensions=('time','y','x'), fill_value = missing_float, zlib=True)
        o.variables[var].long_name     = 'Wind speed at measurement height'
        o.variables[var].standard_name = 'wind_speed'
        o.variables[var].units         = 'm/s'

    return

#############################################################################

if __name__ == "__main__":

    for sitename in sitelist:

        main(sitename)
