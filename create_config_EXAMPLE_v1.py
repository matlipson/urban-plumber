''' This file forms part of the Urban-PLUMBER benchmarking evaluation project 
for urban areas. Copyright 2020. This program is distributed freely in the hope
that it will be useful, but with NO WARRANTY, implied or otherwise. Users are 
responsible for ensuring it is fit for their purpose. '''

__title__   = 'Urban-PLUMBER model configuration script'
__version__ = 'v1.0 (2020-05-22)'
__author__  = 'Mathew Lipson'
__email__   = 'm.lipson@unsw.edu.au'

'''
This python3 script is an example template which converts site parameters into 
a model configuration file, e.g. a fortran namelist. This example will produce 
a configuration based on the requirements of model NOAH-LSM, participants need
to update the script to fit the requirements of their model. The values and 
assumptions are indicative of the method only.

Run the script "as-is" to see output produced in ./XX-Test/output

Instructions:
    1. MAIN: Update number of configuration files required (c1, c2 etc)
    2. INPUT INFORMATION: Update paths to your files
    3. CONFIG FILES: Update the create_config function(s) to your requirements
    4. run script

'''

import numpy as np
import pandas as pd
import netCDF4 as nc

# add sites to list as required
sitelist = ['XX-Test']

###############################################################################
##### 1. MAIN
#####    Update number of configuration files as required (c1, c2 etc)
###############################################################################

def main(sitename):
    print('configuring %s' %sitename)

    print('loading site data and forcing information')
    info = set_info(sitename)
    sitedata,info = set_more_info(info)

    print('creating model configuration files')

    # config 1
    c1 = create_config1(sitedata,info)   
    # write config file   
    f1  = "%s/%s" %(info['outpath'],info['fname_config1'])
    with open(file=f1, mode='w') as ofile:  
        ofile.write(c1)

    # config 2
    c2 = create_config2(sitedata,info)
    # write config file  
    f2 = "%s/%s" %(info['outpath'],info['fname_config2'])
    with open(file=f2, mode='w') as ofile:
        ofile.write(c2)

    print('done! see %s for new files' %info['outpath'])

    return

###############################################################################
##### 2. INPUT INFORMATION:
#####    Input path information.
###############################################################################

def set_info(sitename):
    '''This function sets basic path input information.

    Parameters
    ----------
    sitename (string) : the sitename (set in __main__ scope at end of file)

    Outputs
    -------
    info (dictionary) : information for use by other functions
    '''

    info = {}

    # defines sitename from sitelist
    info['sitename'] = sitename

    # path to site information
    info['sitepath'] = './%s' %sitename

    # path to model configuration (output)
    info['outpath'] = './%s/output' %sitename

    # name of input forcing file
    info['fname_forcing'] = '%s_metforcing_v1.nc' %sitename

    # name of configuration output files (add or remove files as needed)
    info['fname_config1'] = 'EXAMPLE_%s_configfile1.nml' %sitename
    info['fname_config2'] = 'EXAMPLE_%s_configfile2.tbl' %sitename

    return info

###############################################################################
##### 3. CONFIG FILES
#####    Update the create_config function(s)
#####    a) Replace "template" string with your model's default configuration.
#####    b) Define variables that will change from the default configuration.
#####    c) Use those variables to replace values from in the template string.
#####        Avoid hardcoding value changes in the default configuration 
#####        template string, define new variables if required so changes can 
#####        be tracked. Here python3.6 'f-string' functionality is utilised, 
#####        see: https://www.python.org/dev/peps/pep-0498/
###############################################################################

def create_config1(sitedata,info):
    '''create string for model configuration file

    Parameters
    ----------
    sitedata (dataframe):  site configuration information
    info (dictionary):     path and timing information

    Output
    ------
    template (str): full string of namelist configuration'''

    ###############################################################################
    #### define any parameters that change the default configuration

    startdate   = pd.to_datetime(info['time_coverage_start']).strftime('%Y%m%d%H%M') 
    enddate     = pd.to_datetime(info['time_coverage_end']).strftime('%Y%m%d%H%M') 
    loops       = 0  # no looping of spinup data
    outdir      = info['outpath']
    latitude    = sitedata['latitude']
    longitude   = sitedata['longitude']
    timestep    = info['timestep_interval_seconds']
    meas_height = sitedata['measurement_height_above_ground']
    usemonalb   = '.TRUE.'
    alb         = sitedata['average_albedo_at_midday']

    ###############################################################################
    ##### CONFIGURATION TEMPLATE
    ##### REPLACE THE "template" STRING WITH YOUR MODEL'S CONFIGURATION
    ##### Default source:
    ###############################################################################

    template= f'''\
&METADATA_NAMELIST
 startdate             = {startdate}
 enddate               = {enddate}
 loop_for_a_while      = {loops}
 output_dir            = "{outdir}"
 Latitude              = {latitude}
 Longitude             = {longitude}
 Forcing_Timestep      = {timestep}
 Noahlsm_Timestep      = 900
 Sea_ice_point         = .FALSE.
 Soil_layer_thickness  =    0.1            0.3            0.6            1.0
 Soil_Temperature      =  266.0995       274.0445       276.8954       279.9152
 Soil_Moisture         =  0.2981597      0.2940254      0.2713114      0.3070948
 Soil_Liquid           =  0.1611681      0.2633106      0.2713114      0.3070948
 Skin_Temperature      =  263.6909
 Canopy_water          =  3.9353027E-04
 Snow_depth            =  1.0600531E-03
 Snow_equivalent       =  2.0956997E-04
 Deep_Soil_Temperature = 285
 Landuse_dataset       = "USGS"
 Soil_type_index       = 8
 Vegetation_type_index = 7
 Urban_veg_category    = 1
 glacial_veg_category  = 24
 Slope_type_index      = 1
 Max_snow_albedo       = 0.75
 Air_temperature_level = {meas_height}
 Wind_level            = {meas_height} 
 Green_Vegetation_Min  = 0.01
 Green_Vegetation_Max  = 0.96
 Usemonalb             = {usemonalb}
 Rdlai2d               = .FALSE.
 sfcdif_option         = 1
 iz0tlnd               = 0
 Albedo_monthly        = {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb},  {alb}
 Shdfac_monthly        = 0.01,  0.02,   0.07,  0.17,  0.27,  0.58,  0.93,  0.96,  0.65,  0.24,  0.11,  0.02
 lai_monthly           = 4.00,  4.00,   4.00,  4.00,  4.00,  4.00,  4.00,  4.00,  4.00,  4.00,  4.00,  4.00
 Z0brd_monthly         = 0.020, 0.020, 0.025, 0.030, 0.035, 0.036, 0.035, 0.030, 0.027, 0.025, 0.020, 0.020
! Z0brd_monthly         = 0.100, 0.140, 0.180, 0.220, 0.250, 0.280, 0.250, 0.220, 0.180, 0.140, 0.120, 0.100
 Use_urban_module      = .TRUE.
/
&URBAN_NAMELIST
 Num_layers       = 4
 Utype            = 3
 ZLVL_Urban       = {meas_height}
 Lsolar_urb       = .FALSE.
 initial_roof_temperature = 280.0, 282.0, 290.0, 300.0
 initial_wall_temperature = 280.0, 282.0, 290.0, 300.0
 initial_road_temperature = 270.0, 275.0, 276.0, 280.0
/

'''
    return template

###############################################################################

def create_config2(sitedata,info):
    '''create string for model configuration file (if second file required)

    Parameters
    ----------
    sitedata (dataframe):   site configuration information
    info (dictionary):      path and timing information

    Output
    ------
    template (str): full string of namelist configuration


    Example assumptions
    -------------------
    - simple infinite canyon geometry for morphology conversions
    - simple bulk albedo for all factets
    - a fixed proportion of anthropogenic heat is latent heat

    '''

    # direct config values 
    zr      = sitedata['building_mean_height']
    ah      = sitedata['anthropogenic_heat_flux_mean']

    frc_urb = sitedata['impervious_area_fraction']
    albr    = sitedata['average_albedo_at_midday']
    albb    = sitedata['average_albedo_at_midday']
    albg    = sitedata['average_albedo_at_midday']
    
    # interpreted config values
    road_width = round(sitedata['building_mean_height']/sitedata['canyon_height_width_ratio'], 2)
    roof_width = round(sitedata['roof_area_fraction']*road_width/(1.-sitedata['roof_area_fraction']), 2)
    alh        = ah*0.25  # assumption: a quarter of anthropogenic heat is latent


    ###############################################################################
    ##### CONFIGURATION TEMPLATE
    ##### Replace the "template" string with your model's default configuration
    ##### Default source:
    ###############################################################################

    template= f"""\
# The parameters in this table may vary greatly from city to city.
# The default values are probably not appropriate for any given city.
# Users should adapt these values based on the city they are working
# with.

# Urban Parameters depending on Urban type
# USGS

Number of urban categories: 3

#
#  Where there are multiple columns of values, the values refer, in
#  order, to: 1) Low density residential, 2) High density residential, 
#  and 3) Commercial:  I.e.:
#
#  Index:     1           2              3
#  Type:  Low-dens Res, Hi-dens Res, Commercial
#

#
# ZR:  Roof level (building height)  [ m ]
#      (sf_urban_physics=1)

ZR: 5.0,  7.5,  {zr}

#
# SIGMA_ZED:  Standard Deviation of roof height  [ m ]
#      (sf_urban_physics=1)

SIGMA_ZED: 1.0,  3.0,  4.0

#
# ROOF_WIDTH:  Roof (i.e., building) width  [ m ]
#      (sf_urban_physics=1)

ROOF_WIDTH: 8.3, 9.4, {roof_width}

#
# ROAD_WIDTH:  road width  [ m ]
#      (sf_urban_physics=1)
#

ROAD_WIDTH: 8.3, 9.4, {road_width}

#
# AH:  Anthropogenic heat [ W m{-2} ]
#      (sf_urban_physics=1)
#

AH:  20.0, 50.0, {ah}


#
# ALH:  Anthropogenic latent heat [ W m{-2} ]
#      (sf_urban_physics=1)
#

ALH:  20.0, 25.0, {alh}

#
#  AKANDA_URBAN:  Coefficient modifying the Kanda approach to computing
#  surface layer exchange coefficients.
#      (sf_urban_physics=1)

AKANDA_URBAN:  1.29 1.29 1.29

#
# DDZR:  Thickness of each roof layer [ m ]
#        This is currently NOT a function urban type, but a function
#        of the number of layers.  Number of layers must be 4, for now.
#      (sf_urban_physics=1)


DDZR:  0.05, 0.05, 0.05, 0.05

#
# DDZB:  Thickness of each building wall layer [ m ]
#        This is currently NOT a function urban type, but a function
#        of the number of layers.  Number of layers must be 4, for now.
#      (sf_urban_physics=1)
#

DDZB: 0.05, 0.05, 0.05, 0.05

#
# DDZG:  Thickness of each ground (road) layer [ m ]
#        This is currently NOT a function urban type, but a function
#        of the number of layers.  Number of layers must be 4, for now.
#      (sf_urban_physics=1)
#

DDZG: 0.05, 0.25, 0.50, 0.75

#
# BOUNDR:  Lower boundary condition for roof layer temperature [ 1: Zero-Flux,  2: T = Constant ]
#      (sf_urban_physics=1)
#

BOUNDR: 1

#
# BOUNDB:  Lower boundary condition for wall layer temperature [ 1: Zero-Flux,  2: T = Constant ]
#      (sf_urban_physics=1)
#

BOUNDB: 1

#
# BOUNDG:  Lower boundary condition for ground (road) layer temperature [ 1: Zero-Flux,  2: T = Constant ]
#      (sf_urban_physics=1)
#

BOUNDG: 1

#
# Ch of Wall and Road [ 1: M-O Similarity Theory, 2: Empirical Form of Narita et al., 1997 (recommended) ]
#      (sf_urban_physics=1)
#

CH_SCHEME: 2

#
# Surface and Layer Temperatures [ 1: 4-layer model,  2: Force-Restore method ]
#      (sf_urban_physics=1)
#

TS_SCHEME: 1

#
# AHOPTION [ 0: No anthropogenic heating,  1: Anthropogenic heating will be added to sensible heat flux term ]
#      (sf_urban_physics=1)
#

AHOPTION: 0

#
# Anthropogenic Heating diurnal profile.
#   Multiplication factor applied to AH (as defined in the table above)
#   Hourly values ( 24 of them ), starting at 01 hours Local Time.
#   For sub-hourly model time steps, value changes on the hour and is
#   held constant until the next hour.
#      (sf_urban_physics=1)
#

AHDIUPRF: 0.16 0.13 0.08 0.07 0.08 0.26 0.67 0.99 0.89 0.79 0.74 0.73 0.75 0.76 0.82 0.90 1.00 0.95 0.68 0.61 0.53 0.35 0.21 0.18

#
# FRC_URB:  Fraction of the urban landscape which does not have natural
#           vegetation. [ Fraction ]
#      (sf_urban_physics=1,2,3)
#

FRC_URB: 0.5, 0.9, {frc_urb}

#
# CAPR:  Heat capacity of roof  [ J m{-3} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

CAPR: 1.0E6, 1.0E6, 1.0E6,

#
# CAPB:  Heat capacity of building wall [ J m{-3} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

CAPB: 1.0E6, 1.0E6, 1.0E6,

#
# CAPG:  Heat capacity of ground (road) [ J m{-3} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

CAPG:  1.4E6, 1.4E6, 1.4E6,

#
# AKSR:  Thermal conductivity of roof [ J m{-1} s{-1} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

AKSR:  0.67, 0.67, 0.67,

#
# AKSB:  Thermal conductivity of building wall [ J m{-1} s{-1} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

AKSB:  0.67, 0.67, 0.67,

#
# AKSG:  Thermal conductivity of ground (road) [ J m{-1} s{-1} K{-1} ]
#      (sf_urban_physics=1,2,3)
#

AKSG: 0.4004, 0.4004, 0.4004,

#
# ALBR:   Surface albedo of roof [ fraction ]
#      (sf_urban_physics=1,2,3)
#

ALBR: 0.20, 0.20, {albr}

#
# ALBB:  Surface albedo of building wall [ fraction ]
#      (sf_urban_physics=1,2,3)
#

ALBB: 0.20, 0.20, {albb}

#
# ALBG:  Surface albedo of ground (road) [ fraction ]
#      (sf_urban_physics=1,2,3)
#

ALBG: 0.20, 0.20, {albg}

#
# EPSR:  Surface emissivity of roof [ - ]
#      (sf_urban_physics=1,2,3)
#

EPSR: 0.90, 0.90, 0.90

#
# EPSB:  Surface emissivity of building wall [-]
#      (sf_urban_physics=1,2,3)
#

EPSB: 0.90, 0.90, 0.90

#
# EPSG:  Surface emissivity of ground (road) [ - ]
#      (sf_urban_physics=1,2,3)
#

EPSG: 0.95, 0.95, 0.95

#
# Z0B:  Roughness length for momentum, over building wall [ m ]
#       Only active for CH_SCHEME == 1
#      (sf_urban_physics=1)
#

Z0B: 0.0001, 0.0001, 0.0001

#
# Z0G:  Roughness length for momentum, over ground (road) [ m ]
#       Only active for CH_SCHEME == 1
#      (sf_urban_physics=1,2,3)
#

Z0G: 0.01, 0.01, 0.01

#
# Z0R:  Roughness length for momentum over roof [ m ]
#      (sf_urban_physics=2,3)
#

Z0R: 0.01, 0.01, 0.01

#
# TRLEND:  Lower boundary condition for roof temperature [ K ]
#      (sf_urban_physics=1,2,3)
#

TRLEND: 293.00, 293.00, 293.00

#
# TBLEND:  Lower boundary temperature for building wall temperature [ K ]
#      (sf_urban_physics=1,2,3)
#

TBLEND: 293.00, 293.00, 293.00

#
# TGLEND:  Lower boundary temperature for ground (road) temperature [ K ]
#      (sf_urban_physics=1,2,3)
#

TGLEND: 293.00, 293.00, 293.00

#
# COP:  Coefficient of performance of the A/C systems [ - ]
#      (sf_urban_physics=3)
#

COP: 3.5, 3.5, 3.5

#
# PWIN:  Coverage area fraction of windows in the walls of the building [ - ]
#      (sf_urban_physics=3)
#

PWIN: 0.2, 0.2, 0.2

#
# BETA:  Thermal efficiency of heat exchanger
#      (sf_urban_physics=3)
#

BETA: 0.75, 0.75, 0.75

#
# SW_COND:  Air conditioning switch, 1=ON
#      (sf_urban_physics=3)
#

SW_COND: 1, 1, 1

#
# TIME_ON:  Initial local time of A/C systems, [ h ]
#      (sf_urban_physics=3)
#

TIME_ON: 0., 0., 0.

#
# TIME_OFF:  End local time of A/C systems, [ h ]
#      (sf_urban_physics=3)
#

TIME_OFF: 24., 24., 24.

#
# TARGTEMP:  Target Temperature of the A/C systems, [ K ]
#      (sf_urban_physics=3)
#

TARGTEMP: 298., 298., 297.

#
# GAPTEMP:  Comfort Range of the indoor Temperature, [ K ]
#      (sf_urban_physics=3)
#

GAPTEMP: 0.5, 0.5, 0.5

#
# TARGHUM:  Target humidity of the A/C systems, [ Kg/Kg ]
#      (sf_urban_physics=3)
#

TARGHUM: 0.005, 0.005, 0.005

#
# GAPHUM:  Comfort Range of the specific humidity, [ Kg/Kg ]
#      (sf_urban_physics=3)
#

GAPHUM: 0.005, 0.005, 0.005

#
# PERFLO:  Peak number of occupants per unit floor area, [ person/m^2 ]
#      (sf_urban_physics=3)
#

PERFLO: 0.01, 0.01, 0.02

#
# HSEQUIP:  Diurnal heating profile of heat generated by equipments
#      (sf_urban_physics=3)
#

HSEQUIP: 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0.5 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 0.5 0.25 0.25 0.25 0.25 0.25

#
# HSEQUIP_SCALE_FACTOR:  Peak heat generated by equipments, [ W/m^2 ]
#      (sf_urban_physics=3)
#

HSEQUIP_SCALE_FACTOR: 16.00, 20.00, 36.00

STREET PARAMETERS:
#      (sf_urban_physics=2,3)

#  urban      street      street     building
# category  direction     width      width
# [index]  [deg from N]    [m]        [m]

    1         0.0          30.       13.
    1        90.0          30.       13.
    2         0.0          25.       17.
    2        90.0          25.       17.
    3         0.0          20.       20.
    3        90.0          20.       20.

END STREET PARAMETERS

BUILDING HEIGHTS: 1
#      (sf_urban_physics=2,3)

#     height   Percentage
#      [m]       [%]
       5.0      15.0
      10.0      70.0
      15.0      15.0
END BUILDING HEIGHTS

BUILDING HEIGHTS: 2
#      (sf_urban_physics=2,3)

#     height   Percentage
#      [m]       [%]
       5.0        0.0
      10.0       20.0
      15.0       60.0
      20.0       20.0
END BUILDING HEIGHTS

BUILDING HEIGHTS: 3
#      (sf_urban_physics=2,3)

#     height   Percentage
#      [m]       [%]
       5.0       0.0
      10.0       0.0
      15.0      10.0
      20.0      25.0
      25.0      40.0
      30.0      25.0
      35.0       0.0
END BUILDING HEIGHTS

"""
    
    return template

###############################################################################
##### 4. OTHER FUNCTIONS
###############################################################################

def set_more_info(info):
    '''sets timing and other info from forcing file and places into dictionary.

    Inputs
    ------
    info (dictionary): script information

    Outputs
    -------
    info (dictionary): updated script information
    sitedata (dataframe): site information
    '''

    # load site data table
    path_sitedata = '%s/%s_sitedata_v1.csv' %(info['sitepath'], info['sitename'])
    sitedata_full = pd.read_csv(path_sitedata, index_col=1, delimiter=',')
    sitedata      = pd.to_numeric(sitedata_full['value'])

    # load forcing information
    fpath = '%s/%s' %(info['sitepath'],info['fname_forcing'])
    with nc.Dataset(filename=fpath, mode='r', format='NETCDF4') as f:

        info['time_coverage_start']       = f.time_coverage_start
        info['time_coverage_end']         = f.time_coverage_end
        info['time_analysis_start']       = f.time_analysis_start
        info['local_utc_offset_hours']    = f.local_utc_offset_hours
        info['timestep_interval_seconds'] = f.timestep_interval_seconds
        info['timestep_number_spinup']    = f.timestep_number_spinup
        info['timestep_number_analysis']  = f.timestep_number_analysis

    return sitedata, info

###############################################################################
##### __main__  scope
###############################################################################

if __name__ == "__main__":

    for sitename in sitelist:

        main(sitename)

###############################################################################