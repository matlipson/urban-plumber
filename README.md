Urban-PLUMBER
-------------

These files form part of the Urban-PLUMBER benchmarking evaluation project for urban areas. 

You may register to participate by emailing: **met-urban-plumber@lists.reading.ac.uk**

These scripts are provided to help participants automate model configuration and input/output file generation for multiple sites.

Contents
--------

The following example scripts will work for any site in Urban-PLUMBER:

- **create_config_EXAMPLE_v1.py**: automates model configuration by reading the provided site data file, making necessary conversions and outputting namelists for an example model.
- **create_forcing_EXAMPLE_v1.py**: takes the netCDF forcing file, makes variable conversions and creates a padded text forcing file in the format required for an example model. 
- **create_netcdf_EXAMPLE_v1.py**: takes dummy model output and constructs a single netCDF with all requested variables and metadata.

The following data files are an example test site (XX-Test) with data for only 7 days.

- **XX-Test/XX-Test_sitedata_v1.csv**: comma-separated text file with site characteristic information
- **XX-Test/XX-Test_metforcing_v1.nc**: meteorological forcing data in netCDF format
- **XX-Test/XX-Test_metforcing_v1.txt**: equivalent forcing data in space-separated text format
- **XX-Test/ouptut/modeloutput_EXAMPLE.txt**: example model output (7 days)
- **XX-Test/output/EXAMPLE_XX-Test_baseline_v1.txt**: example model subgrid parameters

Full site data can be accessed on modelevaluation.org after registering.

Instructions
------------

These are python3 scripts and require numpy, pandas and netcdf packages. These can be installed with [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html):

`conda install python numpy pandas netcdf4`

To produce example output for the XX-Test site, clone or download the repository and type:

```
python create_config_EXAMPLE_v1.py
python create_forcing_EXAMPLE_v1.py
python create_netcdf_EXAMPLE_v1.py
```

Output can be uploaded to modelevaluation.org as a test case which will not be assessed.

More detailed instructions are contained in each script.

These files are distributed freely in the hope that it will be useful, but with NO WARRANTY, implied or otherwise.
Users are responsible for ensuring they are fit for their purpose. Copyright 2020.

