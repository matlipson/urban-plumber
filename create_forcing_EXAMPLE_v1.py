''' This file forms part of the Urban-PLUMBER benchmarking evaluation project 
for urban areas. Copyright 2020. This program is distributed freely in the hope
that it will be useful, but with NO WARRANTY, implied or otherwise. Users are 
responsible for ensuring it is fit for their purpose. '''

__title__   = 'Urban-PLUMBER model forcing script'
__version__ = 'v1.01 (2020-10-22)'
__author__  = 'Mathew Lipson'
__email__   = 'm.lipson@unsw.edu.au'

'''
This python3 script is an example which converts the standard netcdf forcing 
into a text format appropriate for forcing a model. This example is based on
the offline NOAH-SLUCM forcing requirements, so converts:
    specific humidity, temperature and pressure   ->  relative humidity
    northward and eastward wind components        ->  wind speed
    northward and eastward wind components        ->  wind direction from north
    pressure in Pa                                ->  pressure in h Pa
    rainfall and snowfall                         ->  precipitation

Run the script "as-is" to see output produced in ../XX-Test/output

Instructions: 
    1. INPUT INFORMATION: update paths and forcing file name and options
    2. CONFIG HEADER: add header information for the text forcing file
    3. CONFIG FORCING FORMAT: 
        a) define dataframe column structure
        b) define column formats for output
        c) assign values to columns, and format column widths and dtypes
    4. run script

Options:
    to_local_time: set True if model take local time rather than UTC

Changelog:
    v1.01: Udpate humidity conversion function for higher accuracy at extremes
'''

import numpy as np
import pandas as pd
import netCDF4 as nc

# add sites to list as required
sitelist = ['XX-Test']

###############################################################################
##### MAIN no action required
###############################################################################

def main(sitename):
    print('processing %s' %sitename)

    print('loading site data and forcing information')
    info = set_info(sitename)
    sitedata,info = set_more_info(info)

    print('creating model forcing data')
    data_df, formats = create_forcing(info)
    data_str = data_df.to_string(
        formatters=formats, header=False, index=False)

    print('write out padded forcing file')
    fname = '%s/%s' %(info['outpath'], info['fname_output'])
    with open(fname, 'w') as file:
        file.writelines(data_str)

    print('insert header information in forcing file')
    create_header(fname)

    print('done! see %s/%s' %(info['outpath'], info['fname_output']))

    return

###############################################################################
##### 1. INPUT INFORMATION
#####    update paths and forcing file name and options (UTC or local)
###############################################################################

def set_info(sitename):
    '''This function sets basic path input information.

    Parameters
    ----------
    sitename (string) : the sitename (set from sitelist)

    Outputs
    -------
    info (dictionary) : information for use by other functions
    '''

    info = {}

    info['sitename'] = sitename

    # path to site information
    info['sitepath'] = './%s' %sitename

    # path to model forcing (output)
    info['outpath'] = './%s/output' %sitename

    # name of input forcing file
    info['fname_forcing'] = '%s_metforcing_v1.nc' %sitename

    # name of output forcing file
    info['fname_output'] = 'EXAMPLE_forcing.dat'

    # convert to local time if required by model (set to False if using UTC)
    info['to_local_time'] = False

    return info

###############################################################################
##### 2. CONFIG HEADER: add header information for the text forcing file
###############################################################################

def create_header(fname_output):
    ''' 
    adds custom header to forcing data file

    Parameters
    ----------
    fname_output (string): name of forcing file
    '''

    # add header comments and metainfo
    with open(fname_output, 'r') as f1: # read existing datafile into origdata
        origdata = f1.read()
        with open(fname_output, 'w') as f2: # write new file with header
            f2.write('------------------------------------------------------------------------------------------------------------------------------------------------------------\n')
            f2.write(' UTC date/time        windspeed       wind dir         temperature      humidity        pressure           shortwave      longwave          precipitation   \n')
            f2.write('yyyy mm dd hh mi       m s{-1}        degrees               K               %             hPa               W m{-2}        W m{-2}          kg m{-2} s{-1}  \n')
            f2.write('------------------------------------------------------------------------------------------------------------------------------------------------------------\n')
            f2.write('<Forcing>  This tag ("<Forcing>", not case sensitive) begins the section of forcing data.\n')
            f2.write(f'{origdata}')        # replace origdata after header

    return

###############################################################################
##### 3. CONFIG FORCING FORMAT: define column and data formats
###############################################################################

def create_forcing(info):
    '''
    Parameters
    ----------
    info (dictionary): script information

    Output
    ------
    forcing (dataframe)  : forcing data in model input form
    formats (dictionary) : string formatters for each dataframe column
    '''

    # set timesteps using forcing information
    times = pd.date_range( start = info['time_coverage_start'], 
                           end   = info['time_coverage_end'], 
                           freq  = '%sS' %(info['timestep_interval_seconds']))

    # change to local time if model requries
    if info['to_local_time']:
        offset = pd.Timedelta('%s hours' %info['local_utc_offset_hours'])
        times = times + offset

    # create empty dataframe
    forcing = pd.DataFrame(
        index=times, 
        columns=[
            'yyyy',     # Year [YYYY]
            'mm',       # Month [mm]
            'dd',       # Day [dd]
            'hh',       # Hour [HH]
            'mi',       # Minute [MM]
            'wind',     # Wind speed [m s-1]
            'wdir',     # Wind direction [deg]
            'temp',     # Air temperature [K]
            'rh',       # Relative Humidity [%]
            'psurf',    # Surface pressure [h Pa]
            'sw',       # Shortwave down [W m2]
            'lw',       # Longwave down [W m2]
            'precip',   # precipitation [kg m-2 s-1]
            ])

    # load forcing information
    fpath = '%s/%s' %(info['sitepath'],info['fname_forcing'])
    with nc.Dataset(filename=fpath, mode='r', format='NETCDF4') as f:

        Wind_E = f.variables['Wind_E'][:].flatten().data
        Wind_N = f.variables['Wind_N'][:].flatten().data 
        Tair   = f.variables['Tair'][:].flatten().data
        Qair   = f.variables['Qair'][:].flatten().data
        PSurf  = f.variables['PSurf'][:].flatten().data
        SWdown = f.variables['SWdown'][:].flatten().data
        LWdown = f.variables['LWdown'][:].flatten().data
        Rainf  = f.variables['Rainf'][:].flatten().data
        Snowf  = f.variables['Snowf'][:].flatten().data

    # fill dataframe with forcing data with appropriate conversions
    forcing = forcing.assign(
                        yyyy   = times.year.values,
                        mm     = times.month.values,
                        dd     = times.day.values,
                        hh     = times.hour.values,
                        mi     = times.minute.values,
                        wind   = convert_uv_to_wind(Wind_E,Wind_N),
                        wdir   = convert_uv_to_wdir(Wind_E,Wind_N),
                        temp   = Tair,
                        rh     = convert_qair_to_rh(Qair,Tair,PSurf),
                        psurf  = PSurf/100.,
                        sw     = SWdown,
                        lw     = LWdown,
                        precip = Rainf+Snowf,
                        )

    # set column formats
    # include comma (,) in string if csv required e.g. '{:04d},'
    # for more formatting info see https://pyformat.info
    formats = {
            'yyyy'  : '{:04d}'.format, # zero padded 4-character integer column
            'mm'    : '{:02d}'.format, # zero padded 2-character integer column
            'dd'    : '{:02d}'.format, 
            'hh'    : '{:02d}'.format, 
            'mi'    : '{:02d}'.format, 
            'wind'  : '{:16.10f}'.format, # 16-character, 10 decimal float column
            'wdir'  : '{:16.10f}'.format, 
            'temp'  : '{:16.10f}'.format, 
            'rh'    : '{:16.10f}'.format, 
            'psurf' : '{:16.10f}'.format, 
            'sw'    : '{:16.10f}'.format, 
            'lw'    : '{:16.10f}'.format, 
            'precip': '{:16.10f}'.format
            }

    return forcing, formats

###############################################################################

def set_more_info(info):
    '''sets timing and other info from forcing file and places into info dictionary.

    Inputs
    ------
    info (dictionary): script information

    Outputs
    -------
    info (dictionary): updated script information
    sitedata (dataframe): site information
    '''

    # load site data table
    path_sitedata = '%s/%s_sitedata_v1.csv' %(info['sitepath'], info['sitename'])
    sitedata_full = pd.read_csv(path_sitedata, index_col=1, delimiter=',')
    sitedata      = pd.to_numeric(sitedata_full['value'])

    # load forcing information
    fpath = '%s/%s' %(info['sitepath'],info['fname_forcing'])
    with nc.Dataset(filename=fpath, mode='r', format='NETCDF4') as f:

        info['time_coverage_start']       = f.time_coverage_start
        info['time_coverage_end']         = f.time_coverage_end
        info['time_analysis_start']       = f.time_analysis_start
        info['local_utc_offset_hours']    = f.local_utc_offset_hours
        info['timestep_interval_seconds'] = f.timestep_interval_seconds
        info['timestep_number_spinup']    = f.timestep_number_spinup
        info['timestep_number_analysis']  = f.timestep_number_analysis

    return sitedata, info

###############################################################################
##### OTHER FUNCTIONS
###############################################################################

def convert_uv_to_wdir(u,v):
    ''' Converts 2 component wind velocity to wind direction scalar
    Parameters
    ----------
    u (np array):    eastward wind component     [m/s]
    v (np array):    northward wind component    [m/s]

    Returns
    ------
    wdir (np array): wind direction from North  [deg]
    '''

    wdir = (180./np.pi)*np.arctan2(u, v)
    # convert negative angles to positive
    wdir = np.where(wdir<0, wdir+360., wdir)

    return wdir

def convert_uv_to_wind(u,v):
    ''' Converts 2 component wind velocity to wind speed scalar
    Parameters
    ----------
    u (np array):    eastward wind component     [m/s]
    v (np array):    northward wind component    [m/s]

    Returns
    ------
    wind (np array): wind speed scalar          [m/s]
    '''

    wind = np.sqrt(u**2 + v**2)

    return wind

def convert_qair_to_rh(qair,temp,pressure):
    '''Converts specific humitity to relative humidity 
    using equations from Weedon 2010 & Cucchi 2020.

    Parameters
    ----------
    temp           air temperature    [K]
    pressure       air pressure       [Pa]

    Returns
    -------
    rh            relative humidity   [%]
    '''

    # calculate vapor pressure and specific humidity at saturation
    esat,qsat = calc_sat(temp,pressure)

    # calculate relative humidity
    rh = 100.*qair/qsat

    return rh

def calc_sat(temp,pressure):
    '''Calculates vapor pressure at saturation

    From Weedon 2010 via Buck 1981: 
    New Equations for Computing Vapor Pressure and Enhancement Factor, Journal of Applied Meteorology

    Parameters
    ----------
    temp        [K]     2m air temperature
    pressure    [Pa]    air pressure

    Returns
    -------
    esat (np array):  vapor pressure at saturation     [Pa]
    '''
   
    # constants
    Rd = 287.058  # specific gas constant for dry air
    Rv = 461.52  #specific gas constant for water vapour
    Epsilon = Rd/Rv  # = 0.622...
    Beta = (1.-Epsilon) # = 0.378 ...

    temp_C = temp - 273.15  # temperature conversion to [C]

    # values when over:         water,  ice
    A = np.where( temp_C > 0., 6.1121,  6.1115 )
    B = np.where( temp_C > 0., 18.729,  23.036 )
    C = np.where( temp_C > 0., 257.87,  279.82 )
    D = np.where( temp_C > 0., 227.3,   333.7 )
    X = np.where( temp_C > 0., 0.00072, 0.00022 )
    Y = np.where( temp_C > 0., 3.2E-6,  3.83E-6 )
    Z = np.where( temp_C > 0., 5.9E-10, 6.4E-10 )

    esat = A * np.exp( ((B - (temp_C/D) ) * temp_C)/(temp_C + C))

    enhancement = 1. + X + pressure/100. * (Y + (Z*temp_C**2))

    esat = esat*enhancement*100.

    qsat = (Epsilon*esat)/(pressure - Beta*esat)

    return esat, qsat

###############################################################################
##### __main__  scope
###############################################################################

if __name__ == "__main__":

    for sitename in sitelist:

        main(sitename)

###############################################################################

